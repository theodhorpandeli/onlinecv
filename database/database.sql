-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2015 at 09:42 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database`
--
CREATE DATABASE IF NOT EXISTS `database` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `database`;

-- --------------------------------------------------------

--
-- Table structure for table `info_aftesite`
--

CREATE TABLE IF NOT EXISTS `info_aftesite` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `aftesi_komunikuese` varchar(50) NOT NULL,
  `aftesi_organizative` varchar(50) NOT NULL,
  `aftesi_profesionale` varchar(50) NOT NULL,
  `user_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `info_aftesite`
--

INSERT INTO `info_aftesite` (`id`, `aftesi_komunikuese`, `aftesi_organizative`, `aftesi_profesionale`, `user_id`) VALUES
(24, 'Teadm', 'TeamKot', 'Team', 35),
(25, '', '', '', 35),
(26, '', '', '', 35),
(27, '', '', '', 36),
(28, '', '', '', 37);

-- --------------------------------------------------------

--
-- Table structure for table `info_arsimi`
--

CREATE TABLE IF NOT EXISTS `info_arsimi` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `institucioni` varchar(50) NOT NULL,
  `periudha` varchar(20) NOT NULL,
  `pershkrimi` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `info_arsimi`
--

INSERT INTO `info_arsimi` (`id`, `institucioni`, `periudha`, `pershkrimi`, `user_id`) VALUES
(20, 'UPT', '2012-2015', 'Inxhinieri Informatike', 35),
(21, '', '', '', 35),
(22, '', '', '', 35),
(23, '', '', '', 36),
(24, '', '', '', 37);

-- --------------------------------------------------------

--
-- Table structure for table `info_gjuhet`
--

CREATE TABLE IF NOT EXISTS `info_gjuhet` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `gjuha` varchar(20) NOT NULL,
  `writing` varchar(50) NOT NULL,
  `speaking` varchar(50) NOT NULL,
  `reading` varchar(50) NOT NULL,
  `user_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `info_gjuhet`
--

INSERT INTO `info_gjuhet` (`id`, `gjuha`, `writing`, `speaking`, `reading`, `user_id`) VALUES
(20, 'Anglisht', '10', '10', '10', 35),
(21, '', '', '', '', 35),
(22, '', '', '', '', 35),
(23, '', '', '', '', 36),
(24, '', '', '', '', 37);

-- --------------------------------------------------------

--
-- Table structure for table `info_personale`
--

CREATE TABLE IF NOT EXISTS `info_personale` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `user_id` int(2) NOT NULL,
  `user_name` varchar(15) NOT NULL,
  `user_surname` varchar(15) NOT NULL,
  `user_email` varchar(20) NOT NULL,
  `user_tel` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `info_personale`
--

INSERT INTO `info_personale` (`id`, `user_id`, `user_name`, `user_surname`, `user_email`, `user_tel`) VALUES
(21, 35, '', '', '', ''),
(22, 35, '', '', '', ''),
(23, 35, '', '', '', ''),
(24, 36, '', '', '', ''),
(25, 37, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `info_puna`
--

CREATE TABLE IF NOT EXISTS `info_puna` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `roli_punes` varchar(50) NOT NULL,
  `punedhenesi` varchar(50) NOT NULL,
  `periudha_puna` varchar(50) NOT NULL,
  `pershkrimi_puna` varchar(50) NOT NULL,
  `user_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `info_puna`
--

INSERT INTO `info_puna` (`id`, `roli_punes`, `punedhenesi`, `periudha_puna`, `pershkrimi_puna`, `user_id`) VALUES
(20, 'Programues', 'Ionian', '2015-2016', 'IonianCo', 35),
(21, 'Programues', 'Ionian', '2015-2016', 'IonianCo', 35),
(22, 'Programues', 'Ionian', '2015-2016', 'IonianCo', 35),
(23, '', '', '', '', 36),
(24, '', '', '', 'Informatike', 37);

-- --------------------------------------------------------

--
-- Table structure for table `prove`
--

CREATE TABLE IF NOT EXISTS `prove` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(10) NOT NULL,
  `user_prove` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `prove`
--

INSERT INTO `prove` (`id`, `user_id`, `user_prove`) VALUES
(49, '35', ''),
(50, '35', ''),
(51, '35', ''),
(52, '36', ''),
(53, '37', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `user_Name` varchar(25) NOT NULL,
  `user_Surname` varchar(25) NOT NULL,
  `user_email` varchar(25) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  `user_cel` int(25) NOT NULL,
  `user_gjinia` varchar(15) NOT NULL,
  `user_dat_vit` varchar(20) NOT NULL,
  `user_dat_muaj` varchar(10) NOT NULL,
  `user_dat_dita` varchar(5) NOT NULL,
  `user_aprovuar` varchar(3) NOT NULL,
  `cv_status` varchar(10) NOT NULL,
  `user_admin` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`),
  FULLTEXT KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_Name`, `user_Surname`, `user_email`, `user_password`, `user_cel`, `user_gjinia`, `user_dat_vit`, `user_dat_muaj`, `user_dat_dita`, `user_aprovuar`, `cv_status`, `user_admin`) VALUES
(35, 'theodhorpandeli', 'Theodhor', 'Pandeli', 'theodhorpandeli@gmail.com', '123456789', 693221811, 'Mashkull', '1993', 'Mars', '3', '1', 'kerkoncv', '0'),
(37, 'user', 'User', 'UserMbiemri', 'user@gmail.com', 'useruser', 485833, 'Mashkull', '1984', 'Prill', '15', '1', 'jepmcv', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
